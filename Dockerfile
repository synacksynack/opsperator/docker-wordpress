FROM opsperator/php

# WordPress image for OpenShift Origin

ARG DO_UPGRADE=
ENV WORDPRESS_VERSION=6.0.2

LABEL io.k8s.description="WordPress is a Blog solution." \
      io.k8s.display-name="Wordpress $WORDPRESS_VERSION" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="wordpress,wordpress5" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-wordpress" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$WORDPRESS_VERSION"

USER root

COPY config/* /

RUN set -ex \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get -y update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade WordPress Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install WordPress Dependencies" \
    && apt-get install -y --no-install-recommends ghostscript openssl libgd3 \
	mariadb-client ldap-utils unzip less libpng16-16 libjpeg62-turbo \
	libwebp6 sendmail \
    && savedAptMark="$(apt-mark showmanual)" \
    && apt-get install -y --no-install-recommends libfreetype6-dev \
	libjpeg-dev libmagickwand-dev libpng-dev libzip-dev libldap2-dev \
	libwebp-dev \
    && docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp \
	--with-png \
    && docker-php-ext-configure ldap --with-libdir="lib/$debMultiarch" \
    && docker-php-ext-install -j "$(nproc)" bcmath exif gd ldap opcache zip \
	mysqli \
    && pecl install imagick-3.7.0 \
    && docker-php-ext-enable imagick \
    && apt-mark auto '.*' >/dev/null \
    && apt-mark manual $savedAptMark \
    && echo "# Configure Apache runtime" \
    && a2enmod rewrite expires \
    && find /etc/apache2 -type f -name '*.conf' -exec sed -ri \
	's/([[:space:]]*LogFormat[[:space:]]+"[^"]*)%h([^"]*")/\1%a\2/g' '{}' + \
    && mv /*.ini /usr/local/etc/php/conf.d/ \
    && echo "# Install WordPress" \
    && curl -o wordpress.tar.gz -fSL \
	"https://wordpress.org/wordpress-$WORDPRESS_VERSION.tar.gz" \
    && tar -xzf wordpress.tar.gz -C /usr/src/ \
    && echo "# Install WordPress CLI" \
    && curl -o /usr/local/bin/wp \
	https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar \
    && echo "# Fixing Permissions" \
    && chmod +x /usr/local/bin/wp \
    && for dir in /usr/src/wordpress /etc/lemonldap-ng; \
	do \
	    mkdir -p $dir 2>/dev/null \
	    && chown -R 1001:root $dir \
	    && chmod -R g=u $dir; \
	done \
    && echo "# Cleaning up" \
    && ldd "$(php -r 'echo ini_get("extension_dir");')"/*.so \
	| awk '/=>/ { print $3 }' | sort -u | xargs -r dpkg-query -S \
	| cut -d: -f1 | sort -u | xargs -rt apt-mark manual \
    && apt-get purge -y --auto-remove \
	-o APT::AutoRemove::RecommendsImportant=false \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
	/usr/src/php.tar.xz wordpress.tar.gz \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

CMD "/usr/sbin/apache2ctl" "-D" "FOREGROUND"
ENTRYPOINT ["dumb-init","--","/run-wordpress.sh"]
USER 1001
