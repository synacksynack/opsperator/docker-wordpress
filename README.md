# k8s WordPress

WordPress image.

Diverts from https://gitlab.com/synacksynack/opsperator/docker-php

Build with:

```
$ make build
```

Start Demo or Cluster in OpenShift:

```
$ make ocdemo
$ make ocprod
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name           |    Description             | Default                                                     | Inherited From    |
| :------------------------- | -------------------------- | ----------------------------------------------------------- | ----------------- |
|  `APACHE_DOMAIN`           | WordPress ServerName       | `$WORDPRESS_SERVER_NAME`                                    | opsperator/apache |
|  `APACHE_HTTP_PORT`        | WordPress HTTP(s) Port     | `8080`                                                      | opsperator/apache |
|  `AUTH_METHOD`             | WordPress Users Auth       | `none` (or `ldap`, `header`, `lemon`)                       | opsperator/apache |
|  `LEMON_PROTO`             | LemonLADP-NG CAS Proto     | `${PUBLIC_PROTO}`                                           |                   |
|  `LLNG_SSO_DOMAIN`         | LemonLDAP-NG Portal        | `auth.${OPENLDAP_DOMAIN}`                                   |                   |
|  `OPENLDAP_BASE`           | OpenLDAP Base              | seds `OPENLDAP_DOMAIN`, default produces `dc=demo,dc=local` | opsperator/apache |
|  `OPENLDAP_BIND_DN_RREFIX` | OpenLDAP Bind DN Prefix    | `cn=wordpress,ou=services`                                  | opsperator/apache |
|  `OPENLDAP_BIND_PW`        | OpenLDAP Bind Password     | `secret`                                                    | opsperator/apache |
|  `OPENLDAP_CONF_DN_RREFIX` | OpenLDAP Conf DN Prefix    | `cn=lemonldap,ou=config`                                    | opsperator/apache |
|  `OPENLDAP_DOMAIN`         | OpenLDAP Domain Name       | `demo.local`                                                | opsperator/apache |
|  `OPENLDAP_HOST`           | OpenLDAP Backend Address   | `127.0.0.1`                                                 | opsperator/apache |
|  `OPENLDAP_ORGNAME`        | OpenLDAP Organization Name | `AwesomeDemo`                                               |                   |
|  `OPENLDAP_PORT`           | OpenLDAP Bind Port         | `389` or `636` depending on `OPENLDAP_PROTO`                | opsperator/apache |
|  `OPENLDAP_PROTO`          | OpenLDAP Proto             | `ldap`                                                      | opsperator/apache |
|  `PASSWORDS_DOMAIN`        | SelfServicePassword Domain | `password.${OPENLDAP_DOMAIN}`                               |                   |
|  `PHP_ERRORS_LOG`          | PHP Errors Logs Output     | `/proc/self/fd/2`                                           | opsperator/php    |
|  `PHP_MAX_EXECUTION_TIME`  | PHP Max Execution Time     | `30` seconds                                                | opsperator/php    |
|  `PHP_MAX_FILE_UPLOADS`    | PHP Max File Uploads       | `20`                                                        | opsperator/php    |
|  `PHP_MAX_POST_SIZE`       | PHP Max Post Size          | `8M`                                                        | opsperator/php    |
|  `PHP_MAX_UPLOAD_FILESIZE` | PHP Max Upload File Size   | `2M`                                                        | opsperator/php    |
|  `PHP_MEMORY_LIMIT`        | PHP Memory Limit           | `-1` (no limitation)                                        | opsperator/php    |
|  `PUBLIC_PROTO`            | WordPress HTTP Proto       | `http`                                                      |                   |
|  `SMTP_HOST`               | SMTP Host                  | undef                                                       |                   |
|  `SMTP_PORT`               | SMTP Port                  | `25`                                                        |                   |
|  `WORDPRESS_ADMIN_PASS`    | WordPress Admin Password   | `secret`                                                    |                   |
|  `WORDPRESS_ADMIN_USER`    | WordPress Admin Username   | `admin`                                                     |                   |
|  `WORDPRESS_DB_CHARSET`    | WordPress Database Charset | `utf8`                                                      |                   |
|  `WORDPRESS_DB_NAME`       | WordPress Database Name    | `wordpress`                                                 |                   |
|  `WORDPRESS_DB_HOST`       | WordPress Database Host    | `127.0.0.1`                                                 |                   |
|  `WORDPRESS_DB_PASSWORD`   | WordPress Database Pass    | `secret`                                                    |                   |
|  `WORDPRESS_DB_PORT`       | WordPress Database Port    | `3306`                                                      |                   |
|  `WORDPRESS_DB_USER`       | WordPress Database User    | `wordpress`                                                 |                   |
|  `WORDPRESS_SERVER_NAME`   | WordPress Server Name      | `blog.demo.local`                                           |                   |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point | Description                     | Inherited From    |
| :------------------ | ------------------------------- | ----------------- |
|  `/certs`           | Apache Certificate (optional)   | opsperator/apache |
|  `/var/www/html`    | WordPress Site Root             |                   |
