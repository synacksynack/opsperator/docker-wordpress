#!/bin/sh

if test "$DEBUG"; then
    set -x
    DO_DEBUG=true
else
    DO_DEBUG=false
fi
. /usr/local/bin/nsswrapper.sh

APACHE_HTTP_PORT=${APACHE_HTTP_PORT:-8080}
AUTH_METHOD=${AUTH_METHOD:-none}
OPENLDAP_BIND_DN_PREFIX="${OPENLDAP_BIND_DN_PREFIX:-cn=wordpress,ou=services}"
OPENLDAP_BIND_PW="${OPENLDAP_BIND_PW:-secret}"
OPENLDAP_CONF_DN_PREFIX="${OPENLDAP_CONF_DN_PREFIX:-ou=lemonldap,ou=config}"
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
OPENLDAP_HOST=${OPENLDAP_HOST:-127.0.0.1}
OPENLDAP_ORGNAME="${OPENLDAP_ORGNAME:-AwesomeDemo}"
OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
PUBLIC_PROTO=${PUBLIC_PROTO:-http}
SMTP_PORT=${SMTP_PORT:-25}
WORDPRESS_ADMIN_USER="${WORDPRESS_ADMIN_USER:-admin}"
WORDPRESS_ADMIN_PASS="${WORDPRESS_ADMIN_PASS:-secret}"
WORDPRESS_DB_CHARSET=${WORDPRESS_DB_CHARSET:-utf8}
WORDPRESS_DB_NAME=${WORDPRESS_DB_NAME:-wordpress}
WORDPRESS_DB_HOST=${WORDPRESS_DB_HOST:-wordpress-mysql}
WORDPRESS_DB_PASSWORD="${WORDPRESS_DB_PASSWORD:-secret}"
WORDPRESS_DB_PORT=${WORDPRESS_DB_PORT:-3306}
WORDPRESS_DB_USER="${WORDPRESS_DB_USER:-wordpress}"
if test -z "$OPENLDAP_BASE"; then
    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
fi
if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
    OPENLDAP_PORT=636
elif test -z "$OPENLDAP_PORT"; then
    OPENLDAP_PORT=389
fi
if test -z "$LLNG_SSO_DOMAIN"; then
    LLNG_SSO_DOMAIN=auth.$OPENLDAP_DOMAIN
fi
test -z "$WORDPRESS_SERVER_NAME" && WORDPRESS_SERVER_NAME=blog.$OPENLDAP_DOMAIN
APACHE_DOMAIN=$WORDPRESS_SERVER_NAME
if test -z "$PASSWORDS_DOMAIN"; then
    PASSWORDS_DOMAIN=password.$OPENLDAP_DOMAIN
fi
if test -z "$PWCHANGEMSG"; then
    PWCHANGEMSG="To change your password, go to https://$PASSWORDS_DOMAIN"
fi

cpt=0
echo Waiting for MySQL backend ...
while true
do
    if echo SHOW TABLES | mysql -u "$WORDPRESS_DB_USER" \
	    --password="$WORDPRESS_DB_PASSWORD" -h "$WORDPRESS_DB_HOST" \
	    -P "$WORDPRESS_DB_PORT" "$WORDPRESS_DB_NAME" >/dev/null 2>&1; then
	echo " MySQL is alive!"
	break
    elif test "$cpt" -gt 25; then
	echo Could not reach MySQL >&2
	exit 1
    fi
    echo MySQL ... KO
    sleep 5
    cpt=`expr $cpt + 1`
done

install_from_src()
{
    cp -pr /usr/src/wordpress/* .
    echo "Complete! WordPress has been successfully copied to $PWD"
    if ! test -e .htaccess; then
	echo Initializing htaccess
	cat >.htaccess <<EOF
<IfModule mod_rewrite.c>
    RewriteEngine On
    RewriteBase /
    RewriteRule ^index\.php$ - [L]
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_URI} !=/server-status
    RewriteRule . /index.php [L]
</IfModule>
EOF
    fi
}

set_db_config()
{
    found=$(echo "SELECT option_id, option_value FROM options WHERE option_name = \"$1\";" \
		| mysql -u "$WORDPRESS_DB_USER" \
		    --password="$WORDPRESS_DB_PASSWORD" -h "$WORDPRESS_DB_HOST" \
		    -P "$WORDPRESS_DB_PORT" "$WORDPRESS_DB_NAME" -s 2>/dev/null)
    if test "$found"; then
	foundid=`echo "$found" | cut -f1`
	foundval=`echo "$found" | sed "s|^$found[ \t]*||"`
	if ! test "$foundval" = "$2"; then
	    echo "updating options::$1"
	    echo "UPDATE options SET option_value = \"$2\" WHERE option_id = $foundid;" \
		| mysql -u "$WORDPRESS_DB_USER" \
		    --password="$WORDPRESS_DB_PASSWORD" -h "$WORDPRESS_DB_HOST" \
		    -P "$WORDPRESS_DB_PORT" "$WORDPRESS_DB_NAME" 2>/dev/null
	fi
    else
	echo "adding options::$1"
	echo "INSERT INTO options (option_name, option_value) VALUES (\"$1\", \"$2\")" \
	    | mysql -u "$WORDPRESS_DB_USER" \
		--password="$WORDPRESS_DB_PASSWORD" -h "$WORDPRESS_DB_HOST" \
		-P "$WORDPRESS_DB_PORT" "$WORDPRESS_DB_NAME" 2>/dev/null
    fi
}

if ! test -e index.php -a -e wp-includes/version.php; then
    echo "WordPress not found in $PWD - copying now..."
    if test -n "$(ls -A)"; then
	echo "WARNING: $PWD is not empty! (copying anyway)"
    fi
    install_from_src
fi
if ! test -e wp-config.php; then
    should_init=true
    should_upgrade=false
else
    should_init=false
    should_upgrade=false
    vleft=$(awk '/wp_version/{print $3}' ./wp-includes/version.php | sed -e "s|[';]||g" | tail -1)
    vright=$(awk '/wp_version/{print $3}' /usr/src/wordpress/wp-includes/version.php | sed -e "s|[';]||g" | tail -1)
    majl=$(echo $vleft | cut -d. -f1)
    minl=$(echo $vleft | cut -d. -f2)
    pl=$(echo $vleft | cut -d. -f3)
    majr=$(echo $vright | cut -d. -f1)
    minr=$(echo $vright | cut -d. -f2)
    pr=$(echo $vright | cut -d. -f3)
    if test "$majr" -gt "$majl"; then
	should_upgrade=true
    elif test "$majr" -eq "$majl"; then
	if test "$minr" -gt "$minl"; then
	    should_upgrade=true
	elif test "$minr" -eq "$minl"; then
	    if test "$pr" -a -z "$pl"; then
		should_upgrade=true
	    elif test "$pr" -a "$pl"; then
		if test "$pr" -gt "$pl"; then
		    should_upgrade=true
		fi
	    fi
	fi
    fi
fi

export APACHE_DOMAIN
export APACHE_HTTP_PORT
export PUBLIC_PROTO
SSL_INCLUDE=no-ssl
. /usr/local/bin/reset-tls.sh
export RESET_TLS=false

echo INFO: generating WordPress configuration
sed -e "s|PLACEHOLDER_DB_CHARSET|$WORDPRESS_DB_CHARSET|" \
    -e "s|PLACEHOLDER_DB_COLLATE|$WORDPRESS_DB_COLLATE|" \
    -e "s|PLACEHOLDER_DB_HOST|$WORDPRESS_DB_HOST|" \
    -e "s|PLACEHOLDER_DB_NAME|$WORDPRESS_DB_NAME|" \
    -e "s|PLACEHOLDER_DB_PASSWORD|$WORDPRESS_DB_PASSWORD|" \
    -e "s|PLACEHOLDER_DB_PORT|$WORDPRESS_DB_PORT|" \
    -e "s|PLACEHOLDER_DB_USER|$WORDPRESS_DB_USER|" \
    -e "s|PLACEHOLDER_AUTH_KEY|$WORDPRESS_AUTH_KEY|" \
    -e "s|PLACEHOLDER_AUTH_SALT|$WORDPRESS_AUTH_SALT|" \
    -e "s|PLACEHOLDER_LOGGED_IN_KEY|$WORDPRESS_LOGGED_IN_KEY|" \
    -e "s|PLACEHOLDER_LOGGED_IN_SALT|$WORDPRESS_LOGGED_IN_SALT|" \
    -e "s|PLACEHOLDER_NONCE_KEY|$WORDPRESS_NONCE_KEY|" \
    -e "s|PLACEHOLDER_NONCE_SALT|$WORDPRESS_NONCE_SALT|" \
    -e "s|PLACEHOLDER_SECURE_AUTH_KEY|$WORDPRESS_SECURE_AUTH_KEY|" \
    -e "s|PLACEHOLDER_SECURE_AUTH_SALT|$WORDPRESS_SECURE_AUTH_SALT|" \
    -e "s|PLACEHOLDER_TABLE_PREFIX|$WORDPRESS_TABLE_PREFIX|" \
    -e "s|PLACEHOLDER_DEBUG|$DO_DEBUG|" \
    /init-config.php >wp-config.php
sed -ri -e 's/\r$//' wp-config* 2>/dev/null || echo nothing to patch
chmod 0640 wp-config.php

if $should_init; then
    if ! wp core install \
	    "--url=$WORDPRESS_SERVER_NAME" \
	    "--title=$OPENLDAP_ORGNAME" \
	    "--admin_user=$WORDPRESS_ADMIN_USER" \
	    "--admin_password=$WORDPRESS_ADMIN_PASS" \
	    "--admin_email=root@$OPENLDAP_DOMAIN"; then
        echo "ERROR: failed initializing site" >&2
	exit 1
    fi
    cat <<EOF

    Blog initialized
	Admin user: $WORDPRESS_ADMIN_USER
	Password: $WORDPRESS_ADMIN_PASS

EOF
fi

if test "$AUTH_METHOD" = lemon; then
    if test -z "$LEMON_PROTO"; then
	LEMON_PROTO=$PUBLIC_PROTO
    fi
    if ! test -d /var/www/html/wp-content/plugins/wp-cassify; then
	if ! wp plugin install wp-cassify; then
	    echo WARNING: failed installing wp-cassify >&2
	fi
    fi
    if test -d /var/www/html/wp-content/plugins/wp-cassify; then
	if wp plugin activate wp-cassify; then
	    set_db_config wp_cassify_base_url \
		"$LEMON_PROTO://$LLNG_SSO_DOMAIN/cas/"
	    set_db_config wp_cassify_protocol_version 2
	    set_db_config wp_cassify_disable_authentication ""
	    set_db_config wp_cassify_create_user_if_not_exist \
		create_user_if_not_exist
	    set_db_config wp_cassify_ssl_cipher 0
	    set_db_config wp_cassify_ssl_check_certificate ""
	    set_db_config wp_cassify_redirect_url_after_logout ""
	    set_db_config wp_cassify_override_service_url ""
	    set_db_config wp_cassify_login_servlet ""
	    set_db_config wp_cassify_logout_servlet ""
	    set_db_config wp_cassify_service_validate_servlet ""
	    set_db_config wp_cassify_xpath_query_to_extact_cas_user ""
	    set_db_config wp_cassify_xpath_query_to_extact_cas_attributes ""
	    set_db_config wp_cassify_attributes_list ""
	    set_db_config wp_cassify_allow_deny_order "allow, deny"
	    set_db_config wp_cassify_autorization_rules ""
	    set_db_config wp_cassify_redirect_url_if_not_allowed ""
	    set_db_config wp_cassify_redirect_url_white_list ""
	    set_db_config wp_cassify_user_role_rules ""
	    set_db_config \
		wp_cassify_user_purge_user_roles_before_applying_rules ""
	    set_db_config wp_cassify_user_attributes_mapping_list ""
	    set_db_config wp_cassify_notifications_smtp_host "$SMTP_HOST"
	    set_db_config wp_cassify_notifications_smtp_port $SMTP_PORT
	    set_db_config wp_cassify_notifications_encryption_type ""
	    set_db_config wp_cassify_notifications_smtp_auth ""
	    set_db_config wp_cassify_notifications_salt ""
	    set_db_config wp_cassify_notifications_priority 3
	    set_db_config wp_cassify_notifications_smtp_user ""
	    set_db_config wp_cassify_notifications_smtp_from ""
	    set_db_config wp_cassify_notifications_smtp_to ""
	    set_db_config wp_cassify_notifications_subject_prefix ""
	    set_db_config wp_cassify_notification_rules ""
	    set_db_config wp_cassify_expiration_rules ""
	    set_db_config wp_cassify_xml_response_dump ""
	fi
    fi
    unset OPENLDAP_BASE OPENLDAP_HOST OPENLDAP_DOMAIN OPENLDAP_BIND_PW \
	OPENLDAP_BIND_DN_PREFIX OPENLDAP_CONF_DN_PREFIX
elif test "$AUTH_METHOD" = ldap -a "$OPENLDAP_HOST"; then
    if ! test -d /var/www/html/wp-content/plugins/wpdirauth; then
	if ! wp plugin install wpdirauth; then
	    echo WARNING: failed installing wpdirauth >&2
	elif test -s /wpDirAuth.php.patch; then
	    (
		cd /
		if ! patch -p0 </wpDirAuth.php.patch; then
		    echo WARNING: failed patching wpdirauth >&2
		fi
	    )
	fi
    fi
    if test -d /var/www/html/wp-content/plugins/wpdirauth; then
	if wp plugin activate wpdirauth; then
	    set_db_config dirAuthEnable 1
	    if test "$OPENLDAP_PROTO" = ldaps; then
		set_db_config dirAuthEnableSsl 1
	    else
		set_db_config dirAuthEnableSsl 0
	    fi
	    set_db_config dirAuthRequireSsl 0
	    set_db_config dirAuthControllers $OPENLDAP_HOST:$OPENLDAP_PORT
	    set_db_config dirAuthBaseDn "ou=users,$OPENLDAP_BASE"
	    set_db_config dirAuthPreBindUser ""
	    set_db_config dirAuthAccountSuffix ""
	    set_db_config dirAuthFilter uid
	    set_db_config dirAuthInstitution "$OPENLDAP_ORGNAME"
	    set_db_config dirAuthLoginScreenMsg "LDAP Authentication"
	    set_db_config dirAuthChangePassMsg "$PWCHANGEMSG"
	    set_db_config dirAuthTOS 0
	    set_db_config dirAuthUseGroups 0
	    set_db_config dirAuthGroups ""
	    set_db_config dirAuthMarketingSSOID ""
	    set_db_config dirAuthPreBindPassword ""
	    if test "$WP_DIRAUTH_COOKIE_MAKER"; then
		set_db_config dirAuthCookieMaker "$WP_DIRAUTH_COOKIE_MAKER"
	    fi
	    set_db_config dirAuthAutoRegister 1
	    set_db_config dirAuthCookieExpire 1
	    echo INFO: wpdirauth configuration initialized
	else
	    echo WARNING: failed activating wpdirauth
	fi
    fi
    unset OPENLDAP_BASE OPENLDAP_HOST OPENLDAP_DOMAIN OPENLDAP_BIND_PW \
	OPENLDAP_BIND_DN_PREFIX OPENLDAP_CONF_DN_PREFIX
elif test "$AUTH_METHOD" = header -a "$WP_REMOTE"; then
    if ! test -d /var/www/html/wp-content/plugins/wpremote; then
	if ! wp plugin install wpremote; then
	    echo WARNING: failed installing wpremote >&2
	fi
    fi
    if test -d /var/www/html/wp-content/plugins/wpremote; then
	if wp plugin activate wpremote; then
	    export OPENLDAP_BASE
	    export OPENLDAP_BIND_DN_PREFIX
	    export OPENLDAP_DOMAIN
	    export OPENLDAP_HOST
	else
	    unset OPENLDAP_BASE OPENLDAP_HOST OPENLDAP_DOMAIN OPENLDAP_BIND_PW \
		OPENLDAP_BIND_DN_PREFIX OPENLDAP_CONF_DN_PREFIX
	    echo WARNING: failed activating wpremote
	fi
    fi
    export APACHE_IGNORE_OPENLDAP=yay
else
    export APACHE_IGNORE_OPENLDAP=yay
fi

if $should_upgrade; then
    install_from_src
    if ! wp core update-db; then
	echo WARNING: failed upgrading database >&2
    fi
fi

echo "Generates WordPress VirtualHost Configuration"
sed -e "s HTTP_HOST $APACHE_DOMAIN g" \
    -e "s HTTP_PORT $APACHE_HTTP_PORT g" \
    -e "s SSL_CONF /etc/apache2/$SSL_INCLUDE.conf g" \
    /vhost.conf >/etc/apache2/sites-enabled/003-vhosts.conf

unset WORDPRESS_DB_NAME WORDPRESS_DB_HOST WORDPRESS_DB_PORT \
    WORDPRESS_DB_PASSWORD WORDPRESS_DB_USER WORDPRESS_DB_TABLEPFX \
    WORDPRESS_DB_CHARSET WORDPRESS_DB_COLLATE PASSWORDS_DOMAIN \
    PWCHANGEMSG WP_REMOTE

. /run-apache.sh
