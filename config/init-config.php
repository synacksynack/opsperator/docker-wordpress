<?php
define('AUTH_KEY',         'PLACEHOLDER_AUTH_KEY');
define('AUTH_SALT',        'PLACEHOLDER_AUTH_SALT');
define('DB_NAME',          'PLACEHOLDER_DB_NAME');
define('DB_USER',          'PLACEHOLDER_DB_USER');
define('DB_HOST',          'PLACEHOLDER_DB_HOST');
define('DB_PORT',          'PLACEHOLDER_DB_PORT');
define('DB_PASSWORD',      'PLACEHOLDER_DB_PASSWORD');
define('DB_CHARSET',       'PLACEHOLDER_DB_CHARSET');
define('DB_COLLATE',       'PLACEHOLDER_DB_COLLATE');
define('LOGGED_IN_KEY',    'PLACEHOLDER_LOGGED_IN_KEY');
define('LOGGED_IN_SALT',   'PLACEHOLDER_LOGGED_IN_SALT');
define('NONCE_KEY',        'PLACEHOLDER_NONCE_KEY');
define('NONCE_SALT',       'PLACEHOLDER_NONCE_SALT');
define('SECURE_AUTH_KEY',  'PLACEHOLDER_SECURE_AUTH_KEY');
define('SECURE_AUTH_SALT', 'PLACEHOLDER_SECURE_AUTH_SALT');
define('WP_DEBUG',          PLACEHOLDER_DEBUG);

$table_prefix = 'PLACEHOLDER_TABLE_PREFIX';
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO'])
    && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
    $_SERVER['HTTPS'] = 'on';
}
if (! defined('ABSPATH')) {
    define('ABSPATH', dirname( __FILE__ ) . '/');
}
require_once(ABSPATH . 'wp-settings.php');
