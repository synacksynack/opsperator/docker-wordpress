SKIP_SQUASH?=1
IMAGE=opsperator/wordpress
FRONTNAME=opsperator
-include Makefile.cust

.PHONY: build
build:
	SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: test
test:
	@@docker rm -f testwordpress || true
	@@docker rm -f testmysql || true
	@@docker run --name testmysql \
	    -e MYSQL_DATABASE=msdb \
	    -e MYSQL_PASSWORD=mspassword \
	    -e MYSQL_ROOT_PASSWORD=msrootpassword \
	    -e MYSQL_USER=msuser \
	    -p 3306:3306 \
	    -d docker.io/centos/mariadb-102-centos7:latest
	@@sleep 10
	@@msip=`docker inspect testmysql | awk '/"IPAddress": "/{print $$2}' | head -1 | cut -d'"' -f2`; \
	echo "Working with mysql=$$msip"; \
	docker run --name testwordpress \
	    -e AUTH_METHOD=none \
	    -e WORDPRESS_DB_HOST=$$msip \
	    -e WORDPRESS_ADMIN_USER=admin \
	    -e WORDPRESS_ADMIN_PASS=secr3t \
	    -e WORDPRESS_AUTH_KEY=y1slqk247k04izc2r6hfmfzihlghz1p8idnmpfu7iiyqa89ggn8ma41t01kyh6lg \
	    -e WORDPRESS_AUTH_SALT=2fe3et2g3nv437quezs8j4hixnu9sr4heos8did25z9worhj0c87o12izh6mfj3r \
	    -e WORDPRESS_DB_NAME=msdb \
	    -e WORDPRESS_DB_PASSWORD=mspassword \
	    -e WORDPRESS_DB_USER=msuser \
	    -e WORDPRESS_LOGGED_IN_KEY=7nkf50w9nw4k7js99pjmdxgsgbkohl4qxybp3c3rkcgwwg3ks45c9z0os2fexp4g \
	    -e WORDPRESS_LOGGED_IN_SALT=4cjajndkfjrk3y7x1tfzjmv2uenr54hv5h5aiz4oyu5gh6mepyc7dxmg6hsa7uxf \
	    -e WORDPRESS_NONCE_KEY=ntexmoauqdu94vjsbcjk7jvw3r6vw16ziyqz6jtzuxz0d5gzpz2l5m8geafbs8q6 \
	    -e WORDPRESS_NONCE_SALT=qqft2yz44og2umrxkptcse4wy32q21ut5isu1w8tefwqzc8nlk5b78z5elc9nzuc \
	    -e WORDPRESS_SECURE_AUTH_KEY=dwyqi6yo3g97vuywrqn01y9dh8jb9j03gt0qarjhln0jbj0h11aypoj0i3i10f45 \
	    -e WORDPRESS_SECURE_NONCE_SALT=e23ll5j0wcrkeof5gxkku1dykix4djwgykrs05jt10vs1q01lkd8uzmt52ccq3g5 \
	    -e WORDPRESS_SERVER_NAME=wordpress.demo.local \
	    -d $(IMAGE)

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in secret service deployment; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc apply -f-
	@@BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		-p "WORDPRESS_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "WORDPRESS_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: occlean
occlean: occheck
	@@oc process -f deploy/openshift/run-persistent.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true
	@@oc process -f deploy/openshift/secret.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true

.PHONY: ocdemoephemeral
ocdemoephemeral: ocbuild
	@@if ! oc describe secret openldap-$(FRONTNAME) >/dev/null 2>&1; then \
	    oc process -f deploy/openshift/secret.yaml \
		-p FRONTNAME=$(FRONTNAME) | oc apply -f-; \
	fi
	@@oc process -f deploy/openshift/run-ephemeral.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc apply -f-

.PHONY: ocdemopersistent
ocdemopersistent: ocbuild
	@@if ! oc describe secret openldap-$(FRONTNAME) >/dev/null 2>&1; then \
	    oc process -f deploy/openshift/secret.yaml \
		-p FRONTNAME=$(FRONTNAME) | oc apply -f-; \
	fi
	@@oc process -f deploy/openshift/run-persistent.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc apply -f-

.PHONY: ocdemo
ocdemo: ocdemoephemeral

.PHONY: ocpurge
ocpurge: occlean
	@@oc process -f deploy/openshift/build.yaml | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream.yaml \
	    | oc delete -f- || true
